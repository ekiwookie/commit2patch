import unittest

from commit2patch import _parse_agrs, main
from common import Commit2PatchException
from vcs import get_patches


class TestCommit2Patch(unittest.IsolatedAsyncioTestCase):
    def test_get_patches(self):
        patches = get_patches(
            _get_urls=lambda: (None, "", 0, 1, "test", "test2"),
            _get_repo_data=lambda item: (
                f"repo_url_{item}",
                f"commit_hash_{item}",
                f"project_name_{item}",
                f"netloc_{item}",
            ),
        )
        assert_dict = {
            ("netloc_1", "project_name_1", "repo_url_1"): ["commit_hash_1"],
            ("netloc_test", "project_name_test", "repo_url_test"): ["commit_hash_test"],
            ("netloc_test2", "project_name_test2", "repo_url_test2"): [
                "commit_hash_test2"
            ],
        }
        self.assertDictEqual(assert_dict, patches)

    def test_get_patches_exception(self):
        def _get_repo_data(data):
            raise Commit2PatchException

        patches = get_patches(
            _get_urls=lambda: ("test", "test2"), _get_repo_data=_get_repo_data
        )
        self.assertDictEqual(patches, {})

    def test_args_parser(self):
        args = _parse_agrs(())
        self.assertEqual(args.workers, 3)
        self.assertEqual(args.repo_dir, "repos")
        self.assertEqual(args.patch_dir, "patches")

        args = _parse_agrs(
            (
                "--workers",
                "5",
                "--repo_dir",
                "test_repos",
                "--patch_dir",
                "test_patches",
            )
        )
        self.assertEqual(args.workers, 5)
        self.assertEqual(args.repo_dir, "test_repos")
        self.assertEqual(args.patch_dir, "test_patches")

    def _map_patch(self, item):
        return {
            f"repo_url_{item}",
            f"commit_hash_{item}",
            f"project_name_{item}",
            f"netloc_{item}",
        }

    def _get_patches(self):
        return {
            (f"net_log_{i}", f"project_name_{i}", f"repo_url_{i}"): (
                f"commit_hash_{i}",
            )
            for i in range(5)
        }

    async def test_main(self):
        class Task:
            def __init__(self, *args, **kwargs): ...
            async def handle(self):
                return True

        result = await main(_get_patches=self._get_patches, task=Task, is_logging=False)
        self.assertEqual(5, result)
        result = await main(_get_patches=lambda: {}, task=Task, is_logging=False)
        self.assertEqual(0, result)
