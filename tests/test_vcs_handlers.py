import unittest
from urllib.parse import urlparse

from common import Commit2PatchException
from vcs_handlers.cgit import CGitUrlHandler
from vcs_handlers.common import UrlHandler
from vcs_handlers.github import GithubUrlHandler
from vcs_handlers.gitlab import GitlabUrlHandler
from vcs_handlers.gitweb import GitwebUrlHandler


class TestUrlHandler:
    _UrlHandler = UrlHandler
    _correct_urls = ("",)
    _assert_repo_urls = ("",)
    _incorrect_urls = ("",)

    def _get_handler(self, url: str) -> UrlHandler:
        handler = self._UrlHandler()
        handler.set_source(url, urlparse(url))
        return handler

    def test_repo_url(self):
        for url, assert_url in zip(self._correct_urls, self._assert_repo_urls):
            handler = self._get_handler(url)
            self.assertEqual(handler.get_repo_url(), assert_url)
        for url in self._incorrect_urls:
            handler = self._get_handler(url)
            self.assertRaises(Commit2PatchException, handler.get_repo_url)

    def test_repo_commit_hash(self):
        for url in self._correct_urls:
            handler = self._get_handler(url)
            self.assertEqual(handler.get_commit_hash(), "hash")
        for url in self._incorrect_urls:
            handler = self._get_handler(url)
            self.assertRaises(Commit2PatchException, handler.get_commit_hash)

    def test_repo_project_name(self):
        for url in self._correct_urls:
            handler = self._get_handler(url)
            self.assertEqual(handler.get_project_name(), "project")
        for url in self._incorrect_urls:
            handler = self._get_handler(url)
            self.assertRaises(Commit2PatchException, handler.get_project_name)


class TestGithubUrlHandler(unittest.TestCase, TestUrlHandler):
    _UrlHandler = GithubUrlHandler
    _correct_urls = ("https://github/maintainer/project/commit/hash",)
    _assert_repo_urls = ("https://github/maintainer/project.git",)
    _incorrect_urls = ("https://github/",)


class TestGitlabUrlHandler(unittest.TestCase, TestUrlHandler):
    _UrlHandler = GitlabUrlHandler
    _correct_urls = ("https://gitlab/maintainer/project/-/commit/hash",)
    _assert_repo_urls = ("https://gitlab/maintainer/project.git",)
    _incorrect_urls = ("https://gitlab/",)


class TestGitwebUrlHandler(unittest.TestCase, TestUrlHandler):
    _UrlHandler = GitwebUrlHandler
    _correct_urls = (
        "https://gitweb/gitweb/?p=project.git;a=commitdiff;h=hash",
        "https://gitweb/gitweb/project.git/commit/hash",
    )
    _assert_repo_urls = ("https://gitweb/git/project.git",)
    _incorrect_urls = ("https://gitlab/",)


class TestCGitUrlHandler(unittest.TestCase, TestUrlHandler):
    _UrlHandler = CGitUrlHandler
    _correct_urls = (
        "http://cgit/cgit/project.git/commit/?id=hash",
        "https://cgit/cgit/qt/project.git/commit/?id=hash",
    )
    _assert_repo_urls = ("https://cgit/git/project.git",)
    _incorrect_urls = ("https://gitlab/",)

    def test_repo_url(self):
        class Response:
            def __init__(self, content):
                self.content = content
                self.ok = True

        class ResponseNotOk:
            def __init__(self):
                self.content = ""
                self.ok = False

        class requests_mock_incorrect:
            @staticmethod
            def get(url, **kwargs):
                return Response("<test>")

        class requests_mock_response_not_ok:
            @staticmethod
            def get(url, **kwargs):
                return ResponseNotOk()

        class requests_mock:
            @staticmethod
            def get(url, **kwargs):
                return Response(
                    """
                    <link rel="vcs-git" href="git://incorrect" />
                    <link rel="vcs-git" href="https://correct" />
                    <link rel="vcs-git" href="ssh://incorrect" />
                    """
                )

        handler = self._get_handler("")
        self.assertRaises(
            Commit2PatchException,
            handler._get_repo_url,
            _requests=requests_mock_response_not_ok,
        )
        self.assertRaises(
            Commit2PatchException,
            handler._get_repo_url,
            _requests=requests_mock_incorrect,
        )
        self.assertEqual(
            handler._get_repo_url(_requests=requests_mock), "https://correct"
        )
