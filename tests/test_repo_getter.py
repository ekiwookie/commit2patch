import unittest
from pathlib import Path
from shutil import rmtree
from tempfile import mkdtemp

from common import Commit2PatchException
from repo_getter import RepoGetter


class SubproccesResponse:
    def __init__(self, code):
        self.returncode = code

    async def wait(self): ...


async def subprocess_with_code(*args, **kwarg):
    return SubproccesResponse(500)


async def subprocces_without_code(*args, **kwarg):
    return SubproccesResponse(False)


class TestRepoGetter(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self._repo_dir = mkdtemp()

    def tearDown(self):
        rmtree(self._repo_dir)

    async def test_clone_repo(self):
        repo_getter = RepoGetter(
            project_name="tesdt",
            clone_path=Path(self._repo_dir) / "unexists_repo",
            repo_url="",
            subprocess=subprocces_without_code,
        )
        repo_getter.pull_repo = lambda: False
        self.assertEqual(await repo_getter.get_repo(), True)

        repo_getter = RepoGetter(
            project_name="tesdt",
            clone_path=Path(self._repo_dir) / "unexists_repo",
            repo_url="",
            subprocess=subprocess_with_code,
        )
        repo_getter.pull_repo = lambda: False
        with self.assertRaises(Commit2PatchException):
            await repo_getter.get_repo()
