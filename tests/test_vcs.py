import unittest

from common import Commit2PatchException
from vcs import get_repo_data


class MockHandler1:
    netlocs = ("test1_1", "test1_2", "test1_3")


class MockHandler2:
    netlocs = ("test2_1", "test2_2", "test2_3")

    def set_source(self, url, parsed_url): ...
    def get_repo_data(self):
        return "ok"


class MockHandler3:
    netlocs = ("test3_1", "test3_2", "test3_3")

    def set_source(self, url, parsed_url): ...
    def get_repo_data(self):
        raise Commit2PatchException


class TestVCS(unittest.TestCase):
    def test_unhadled(self):
        handlers = (MockHandler1(), MockHandler2())
        self.assertRaises(Commit2PatchException, get_repo_data, "test", handlers)

    def test_hadled(self):
        handlers = (MockHandler1(), MockHandler2())
        self.assertEqual(get_repo_data("https://test2_1/", handlers), "ok")

    def test_raise(self):
        handlers = (MockHandler1(), MockHandler2(), MockHandler3())
        self.assertRaises(
            Commit2PatchException, get_repo_data, "https://test3_1/", handlers
        )
