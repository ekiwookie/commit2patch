from pathlib import Path

from common import Commit2PatchException
from patch import Patch
from repo_getter import RepoGetter


class Task:
    """Async task for update repo and create patches"""

    def __init__(
        self,
        project_name: str,
        repo_url: str,
        commit_hashes: str,
        repo_dir: str,
        patch_dir: str,
        _Patch: Patch = Patch,
        _Repo_getter: RepoGetter = RepoGetter,
    ):
        self._project_name = project_name
        self._repo_url = repo_url
        self._commit_hashes = commit_hashes
        self._repo_dir = repo_dir
        self._patch_dir = patch_dir
        self._clone_path = Path(self._repo_dir) / self._project_name
        self._Repo_getter = _Repo_getter
        self._Patch = _Patch

    async def handle(self):
        """Main method

        Gets repo and makes patches
        """
        try:
            await self._Repo_getter(
                self._project_name,
                self._clone_path,
                self._repo_url,
            ).get_repo()
            await self._Patch(
                self._project_name,
                self._commit_hashes,
                self._clone_path,
                self._patch_dir,
            ).make_patches()
            return True
        except Commit2PatchException:
            ...
