from common import Commit2PatchException

from .common import UrlHandler


class GitlabUrlHandler(UrlHandler):
    @property
    def netlocs(self) -> list[str]:
        return (
            "gitlab.com",
            "gitlab.isc.org",
            "gitlab.gnome.org",
            "gitlab.freedesktop.org",
            "gitlab.xfce.org",
        )

    def _get_repo_url(self) -> str:
        path_splited = self._parsed_url.path.split("/")
        try:
            return f"https://{self._parsed_url.netloc}/{path_splited[1]}/{path_splited[2]}.git"
        except IndexError:
            raise Commit2PatchException()

    def _get_commit_hash(self) -> str:
        try:
            return self._parsed_url.path.split("/")[5]
        except IndexError:
            raise Commit2PatchException()

    def _get_project_name(self) -> str:
        try:
            return self._parsed_url.path.split("/")[2]
        except IndexError:
            raise Commit2PatchException()
