from urllib.parse import parse_qs

import requests
from bs4 import BeautifulSoup
from common import Commit2PatchException

from .common import UrlHandler


class CGitUrlHandler(UrlHandler):
    @property
    def netlocs(self) -> list[str]:
        return (
            "git.savannah.gnu.org",
            "code.qt.io",
            "cgit.freedesktop.org",
            "git.gluster.org",
        )

    def _get_repo_url(self, _requests: requests = requests) -> str:
        """CGit has a vaious url patterns, easier to parse correct repo url from web"""

        page = _requests.get(
            self._url,
            headers={
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:123.0) Gecko/20100101 Firefox/123.0"
            },
        )
        if not page.ok:
            raise Commit2PatchException(f"resource {self._url} unavailable")
        soup = BeautifulSoup(page.content, "html.parser")
        for link in soup.find_all("link", rel="vcs-git"):
            if link["href"].startswith("https://"):
                return link["href"]
        raise Commit2PatchException()

    def _get_commit_hash(self) -> str:
        try:
            return parse_qs(self._parsed_url.query)["id"][0]
        except (KeyError, IndexError):
            raise Commit2PatchException()

    def _get_project_name(self) -> str:
        return self._get_project_and_maintainer()[1]

    def _get_project_and_maintainer(self) -> (str, str):
        try:
            path_parts = self._parsed_url.path.split("/")
            if path_parts[1] == "cgit":
                del path_parts[1]
            project_name = path_parts[1] if len(path_parts) == 4 else path_parts[2]
            project_maintainer = "git" if len(path_parts) == 4 else path_parts[1]
            return project_maintainer, project_name.split(".")[0]
        except IndexError:
            raise Commit2PatchException()
