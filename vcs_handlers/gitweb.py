from common import Commit2PatchException

from .common import UrlHandler


class GitwebUrlHandler(UrlHandler):
    @property
    def netlocs(self) -> list[str]:
        return (
            "sourceware.org",
            "git.ffmpeg.org",
            "git.openssl.org",
            "git.postgresql.org",
        )

    def _get_repo_url(self) -> str:
        project = self._get_project_name()
        return f"https://{self._parsed_url.netloc}/git/{project}.git"

    def _get_commit_hash(self) -> str:
        try:
            if self._parsed_url.query:
                return self._get_url_data()["h"]
            else:
                return self._parsed_url.path.split("/")[4]
        except (KeyError, IndexError):
            raise Commit2PatchException()

    def _get_project_name(self) -> str:
        try:
            if self._parsed_url.query:
                return self._get_url_data()["p"].split(".")[0]
            else:
                return self._parsed_url.path.split("/")[2].split(".")[0]
        except (KeyError, IndexError):
            raise Commit2PatchException()

    def _get_url_data(self) -> dict:
        ret = {}
        for param in self._parsed_url.query.split(";"):
            key_value = param.split("=")
            ret[key_value[0]] = key_value[1]
        return ret
