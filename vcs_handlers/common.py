from abc import ABC, abstractmethod, abstractproperty
from urllib.parse import ParseResult

from common import Commit2PatchException


class UrlHandler(ABC):
    """Abstract class for getting repo information from various VCS types"""

    def set_source(self, url: str, parsed_url: ParseResult) -> None:
        self._url = url
        self._parsed_url = parsed_url

    @abstractproperty
    def netlocs(self) -> list[str]:
        """Returns list of net locations than handles by class. Should be overriden"""
        ...

    @abstractmethod
    def _get_repo_url(self) -> str:
        """Return repo url. Should be overriden"""
        ...

    @abstractmethod
    def _get_commit_hash(self) -> str:
        """Return commit hash. Should be overriden"""
        ...

    @abstractmethod
    def _get_project_name(self) -> str:
        """Return project name. Should be overriden"""
        ...

    def get_repo_url(self) -> str:
        try:
            return self._get_repo_url()
        except Commit2PatchException:
            raise Commit2PatchException(
                f"{self.__class__.__name__} cant parse repo url"
            )

    def get_commit_hash(self) -> str:
        try:
            return self._get_commit_hash()
        except Commit2PatchException:
            raise Commit2PatchException(
                f"{self.__class__.__name__} cant parse commit hash"
            )

    def get_project_name(self) -> str:
        try:
            return self._get_project_name()
        except Commit2PatchException:
            raise Commit2PatchException(
                f"{self.__class__.__name__} cant parse project name"
            )

    def get_repo_data(self) -> (str, str, str, str):
        """Base method, returns full repository and commit data"""
        return (
            self.get_repo_url(),
            self.get_commit_hash(),
            self.get_project_name(),
            self._parsed_url.netloc,
        )
