import logging
import sys
from collections import defaultdict
from urllib.parse import urlparse

from common import Commit2PatchException
from vcs_handlers.cgit import CGitUrlHandler
from vcs_handlers.github import GithubUrlHandler
from vcs_handlers.gitlab import GitlabUrlHandler
from vcs_handlers.gitweb import GitwebUrlHandler

logger = logging.getLogger(__name__)


def get_urls() -> list[str]:
    """gets urls from stdin"""
    return sys.stdin.readlines()


def get_repo_data(
    url: str,
    handlers: tuple = (
        GitlabUrlHandler(),
        GithubUrlHandler(),
        CGitUrlHandler(),
        GitwebUrlHandler(),
    ),
) -> (str, str, str):
    """Gets repository info from VCS handler"""

    parsed_url = urlparse(url)
    for handler in handlers:
        if parsed_url.netloc in handler.netlocs:
            try:
                handler.set_source(url, parsed_url)
                return handler.get_repo_data()
            except Commit2PatchException as e:
                raise Commit2PatchException(f"{e} in url {url}")
    raise Commit2PatchException(f"{parsed_url.netloc} hasn't correct handler")


def get_patches(_get_urls=get_urls, _get_repo_data=get_repo_data) -> dict:
    """Gets urls from stdin and returns dict where
    key is tuple of (netloc, project_name, repo_url)
    and value is list of commit hashes"""

    urls = _get_urls()
    patches = defaultdict(lambda: [])
    for url in urls:
        if not url:
            continue
        try:
            repo_url, commit_hash, project_name, netloc = _get_repo_data(url)
        except Commit2PatchException as e:
            logger.warning(e)
            continue
        patches[(netloc, project_name, repo_url)].append(commit_hash)
    return patches
