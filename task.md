# Test assignment

## Task

You are given list of urls pointing to commits on various remote VCS (`commit_urls.txt`). Write a program that downloads repositories from those sources and saves the commits into patch files. Keep in mind that those links are simply test examples, so you sollution must as much general as possible to cover all alike cases from different projects as well.

> **Note**
> 
> One could argue the need to download repository, instead of simply parsing patch from webpage. Let's consider that we have to download repository in any case just to be able to flexibly format patch using `git format-patch` command.

Try to increase script throughput by any of the parallelizm techniques. Argue why you chose your approach.

Patch files need to be named in format `<project>_<commit_sha>.patch`. Provide the program source code as well as instructions for it's usage and requirements needed to launch it. Cover code with unit and integration tests.

### Hint

To create patch from specified commit one can use the following command:

```shell
git format-patch -1 --stdout <commit_sha>
```

## What's being evaluated?

* Python coding skills
* Program performance (Throughput). Optimize in any way you like: you are not li
mited by any of the resourses (cores, RAM, disk space).
* Testing skills (Cases taken into account when designing tests, reasoning behind what tests should be considered unit and what should be considered integrational)
