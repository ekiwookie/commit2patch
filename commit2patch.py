import asyncio
import logging
import os
import sys
from argparse import ArgumentParser

from task import Task
from vcs import get_patches

logger = logging.getLogger(__name__)


def _parse_agrs(args: list[str] | None = None) -> ArgumentParser:
    parser = ArgumentParser(
        description="This program clones repos from various VCS and makes patch files",
        prog="Commit2Patch",
    )
    parser.add_argument(
        "--workers",
        type=int,
        help="active downloading tasks count",
        nargs="?",
        default=3,
    )
    parser.add_argument(
        "--repo_dir",
        type=str,
        help="repositories clone directory",
        nargs="?",
        default="repos",
    )
    parser.add_argument(
        "--patch_dir",
        type=str,
        help="output directory for patch files",
        nargs="?",
        default="patches",
    )
    return parser.parse_args(args)


async def safe_task(
    semaphore, project_name, repo_url, commit_hashes, repo_dir, patch_dir, task
) -> None:
    """Limits concurency tasks"""

    async with semaphore:
        return await task(
            project_name,
            repo_url,
            commit_hashes,
            repo_dir,
            patch_dir,
        ).handle()


async def main(
    args: list[str] | None = None,
    task=Task,
    parse_args=_parse_agrs,
    _get_patches=get_patches,
    is_logging: bool = True,
) -> None:
    """Main function run async tasks

    Parse args from the system or passed on function call.
    Creates a directory for patches if it doesn't exist.
    Starts async tasks for creating patches.
    """

    args = parse_args(args)
    if is_logging:
        logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    os.makedirs(args.patch_dir, exist_ok=True)
    patches = _get_patches()

    if not patches:
        return 0

    semaphore = asyncio.BoundedSemaphore(args.workers)
    tasks = [
        asyncio.ensure_future(
            safe_task(
                semaphore,
                project_name,
                repo_url,
                commit_hashes,
                args.repo_dir,
                args.patch_dir,
                task,
            )
        )
        for (netloc, project_name, repo_url), commit_hashes in patches.items()
    ]
    completed = await asyncio.gather(*tasks)
    completed_count = len(tuple(filter(lambda item: item, completed)))
    return completed_count


if __name__ == "__main__":
    asyncio.run(main())
