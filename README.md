# Commit2Patches

Project gets from stdin list of commit urls and saves patch files from this commits.

Program waits a lot of command from OS (such as updating repo) that's why this operation starts asynchronyously

## Requirements

Program tested on `python3.10`

## Instalation

Use the package manager pip to install requirements:

```bash
pip install -r requirements.txt
```

## Usage

Easiest way to use program is redirect standart input to program:

```bash
python commit2patch.py < <filename.txt>
```

### Configuration

Program can parametrized by passing arguments:

_`--workers`_ - max count of active asynchronyous tasks, default 3

_`--repo_dir`_ - repositories clone directory, default `repos`

_`--patch_dir`_ - output directory for patch files, default `patches`
