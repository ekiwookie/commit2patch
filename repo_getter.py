import asyncio
import logging
import os
import shutil

from common import Commit2PatchException

logger = logging.getLogger(__name__)


class RepoGetter:
    """Class for getting repository from origin"""

    def __init__(
        self,
        project_name: str,
        clone_path: str,
        repo_url: str,
        subprocess=asyncio.create_subprocess_exec,
    ) -> None:
        self._project_name = project_name
        self._repo_url = repo_url
        self._clone_path = clone_path
        self._subprocess = subprocess

    async def get_repo(self) -> bool | None:
        """Basic method for getting repository

        If repo directory exists starts to pull changes. Creates directory if don't exists and clone repo
        """

        try:
            os.makedirs(self._clone_path)
        except FileExistsError:
            await self._pull_repo()
        else:
            await self._clone_repo()
        return True

    async def _clone_repo(self) -> bool | None:
        """Clones repository"""

        process = await self._subprocess(
            "git",
            "clone",
            self._repo_url,
            self._clone_path,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )
        await process.wait()
        if process.returncode:
            logger.info(f"cant clone {self._repo_url}")
            raise Commit2PatchException()
        logger.info(f"{self._repo_url} successfully cloned")
        return True

    async def _pull_repo(self) -> bool | None:
        """Pulls changes

        If cant't pull removes repo's directory and starts clone repo again
        """

        process = await self._subprocess(
            "git",
            "pull",
            "--all",
            cwd=self._clone_path,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )

        await process.wait()
        if process.returncode:
            logger.info(
                f"can't pull changes from {self._clone_path}, removing and trying to clone again"
            )
            shutil.rmtree(self._clone_path)
            return await self.get_repo()
        logger.info(f"{self._clone_path} successfully updated")
        return True
