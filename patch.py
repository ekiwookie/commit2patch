import asyncio
import logging
from pathlib import Path

logger = logging.getLogger(__name__)


class Patch:
    """Patch creating class"""

    def __init__(
        self, project_name, commit_hashes: str, clone_path: str, patch_dir: str
    ) -> None:
        self._commit_hashes = commit_hashes
        self._project_name = project_name
        self._clone_path = clone_path
        self._patch_dir = patch_dir

    async def make_patches(self) -> None:
        """Loop through repository commits and creates all patches"""

        for commit_hash in self._commit_hashes:
            await self._make_patch(commit_hash)

    async def _make_patch(self, commit_hash: str) -> None:
        """Create single patch from commit"""

        commit_file = (
            Path(self._patch_dir) / f"{self._project_name}_{commit_hash}.patch"
        )
        process = await asyncio.create_subprocess_exec(
            "git",
            "format-patch",
            "-1",
            "--stdout",
            commit_hash,
            cwd=self._clone_path,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )
        with open(commit_file, "w") as file:
            for line in await process.communicate():
                if line:
                    file.write(line.decode())
        if process.returncode:
            logger.warning(
                f"error due creating {self._project_name} patch {commit_hash}"
            )
        else:
            logger.info(
                f"successfully created {self._project_name} patch {commit_hash}"
            )
